package fr.greta.formation.voyageDGLV;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class AfriqueDAO {
	
	private Connection conn;
	private String nom;
	private String mail;
	private int Nb_voy;
	
	public AfriqueDAO () throws SQLException{
		
		// Ouverture de la connection. Avec sqlite tres simple just eune creation d'un
		// fichier entre les guillement
		// Pas besoin de mettre une adresse complete avec adresse de serveur, port , ...
		// ex (avec mysql) : jdbc:mysql://localhost/base..
		conn = DriverManager.getConnection("jdbc:sqlite:mada.sqlite");
		Statement st = conn.createStatement();
		// CREATE TABLE .. pour s'assurer qu'il y a creation de table puis
		// demande = Nom de la table
		// id = Pirmary key -> uniq et index
		// ensuite les noms en minuscule correspondent aux info a stocker
		// ci exemple avec majuscule et minscule mais ne sont présent que pour le
		// lisibilité
		st.executeUpdate("CREATE TABLE IF NOT EXISTS demande(" + "id INTEGER PRIMARY KEY," + "nom TEXT, "
				+ "mail TEXT, " + "nb_voy INT)");
	}
	
	public void insereDemande(String n, String m, int v) throws SQLException{
		this.nom = n;
		this.mail = m;
		this.Nb_voy = v;

		// Preparation en vue de stocker des données avec données vide
		// interet:
			// - est de ne pas se compliqué en entrant les values avec quels type de guillemet ...
			// - plus efficace 
		PreparedStatement ps = conn.prepareStatement("INSERT INTO demande(nom, mail, nb_voy) VALUES (?,?,?)");
		
		// Remplissage du statement preparer juste à l'étape du dessus
		ps.setString(1, nom);
		ps.setString(2, mail);
		ps.setInt(3, Nb_voy);
		
		// executeUpdate peut renvoyer un int correspondant au nombre de ligne créée
		int Create_key = ps.executeUpdate();
		System.out.println(String.format("%d viennent d'etre créées", Create_key));
	}
	
	public void afficher_Par_email(String m) throws SQLException{
		// Preparation en vu d'afficher des données de la DB
		//PreparedStatement ps2 = conn.prepareStatement("SELECT * FROM demande");
		//ResultSet rs =ps2.executeQuery();
		//int Compteur = 1;
		
		//bqsonb
		PreparedStatement ps3 = conn.prepareStatement("SELECT * FROM demande WHERE mail = ? ORDER BY nom");
		ps3.setString(1,m);
		ResultSet rs2 =ps3.executeQuery();
		
		
		int Compteur = 1;
		// Ce while permet d'afficher 1 a 1 les demande dans ce cas toutes les infomrations du tableau
		while(rs2.next()) {
			if (Compteur == 1) {
				System.out.println("nom" + "\t" + "Adresse email" + "\t" + "Nombre voyageurs");
				Compteur ++;
			}
			System.out.println(rs2.getString("nom") + "\t" + rs2.getString("mail") + "\t" + rs2.getInt("nb_voy"));
		}
		rs2.close();
		
	}
	
	public void ferme() throws SQLException{
		// Fermeture de la connection
		conn.close();	
	}
	

}
