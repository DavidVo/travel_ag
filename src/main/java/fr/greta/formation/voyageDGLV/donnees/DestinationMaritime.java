package fr.greta.formation.voyageDGLV.donnees;
/// Heritage

public class DestinationMaritime extends Destination
{
	private String ile;
	
	// Definition d'une constante
	private final static String ILE_DEFAULT = null; // Ici constante privée donc utilisable quand dans cette classe
	// mais on peut la mettre public si on veut
	
	
	public String getIle() 
	{
		return ile;
	}

	public void setIle(String ile) 
	{
		this.ile = ile;
	}
	
	/// redefinition de la methode to String pour afficher l'ile
	// Utilisation de "super." pour apl la methode de la classe mere
	
	@Override  // Ajout d'une annontation, donne l'information au compilateur qu'on redefinie une methode
	// permet une verification qu'il n'y a pas d'erreur de typo ou autre
	public String toString()
	{
		return (super.toString() + " (ile de " + ile + ")");
	}
	
	// ou 
	//public String toString()
	//{
	//	return (super.getNoms() + supe.getJours() + ... + " (ile de " + ile + ")");
	//}
	
	// Constructeur sans argument base sur celui de la mere
	public DestinationMaritime()
	{
		super();
		ile = ILE_DEFAULT;
	}
	
	// Constructeur avec 4 arguments base sur celui de la mere
	public DestinationMaritime(String n, String p, int j, String i)
	{
		super(n,p,j);
		this.ile = i;
	}
	
	// Creation d'une methode abstraite (methode sans code)
	//public abstract void rien(); 
	// Pour qu'elle fonctionne il faudrait que le classe soit abstraite en haut : "public abstract class Destin..Mar.."
	// mais si la classe est abstraite on ne peut pas creer de nouveaux abjet avec new
	
}

// Note, Java par defaut creer un constructeur qui permet de creer un objet sans avoir code de constructeur 
// Par contre au moment ou il y a eu codage d'un constructeur, celui fait par java disparait
