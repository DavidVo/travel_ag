package fr.greta.formation.voyageDGLV.donnees;
// Ici creation d'une classe de donnees avec des avec des constructeurs et des getteur et setteur classiques

// ajout de implements indique qu'on utilise l'interace Nomable 

//Ce /** permet de donner des instruction pour le java doc 

/**
 * Cette classe permet de faire des choses susper chouettes
 * @author DGLV
 * @since 11/03/209
 */
public class Destination implements Nomable {
	private String nom;
	private String pays;
	private int jours;
	
	public void Allonger(int Jour_All) {
		this.jours += Jour_All;
	}

	public String toString() {
		return ("Pour " + this.nom + " en " + this.pays + " nous conseillons d'y passer " + this.jours + " jours");
	}

	
	/**
	 * 	Creation d'un constructeur on en fait 2 un qui permet de remplir directement
	 *  et l'autre si jamais les info n'ont pas
	 *  ete indiqué dès le début
	 *  Pour creer un constructeur en Java il faut utiliser le nom de la classe ici
	 *  : Destination
	 *  
	 *  L'ajout de /** avant une classe avec variable permet de mettre automatiquement @param
	 *  
	 * @param n
	 * @param p
	 * @param j
	 */
	

	public Destination(String n, String p, int j) {
		this.nom = n;
		this.pays = p;
		this.jours = j;
	}

	// Appelation d'un constructeur en appelant l'autre en lui donnant des valeurs
	// arbitraire
	// Pour appeler un constructeur dans un constructeur il faut : this()
	public Destination() {
		this("", "", 0);
	}

	public Boolean equals(Destination Compa) {
		return (this.pays.equals(Compa.pays));
	}

	// Creation d'un constructeur permettant de faire un clonage
	public Destination(Destination DesBis) {
		this(DesBis.nom, DesBis.pays, DesBis.jours);
	}

	public String getNom() {
		return nom;
	}

	public String getPays() {
		return pays;
	}
	
	public int getJours() {
		return jours;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

	public void setJours(int jours) {
		this.jours = jours;
	}

	// Pas toujours affiché par eclipse pour diverses raisons
	// Pas souvent utile de de faire un destruteur dans java
	public void finalize() {
		System.out.println("GC is coming");
	}

	
}
