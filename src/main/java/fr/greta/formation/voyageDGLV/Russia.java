package fr.greta.formation.voyageDGLV;

import java.time.LocalDateTime;
import java.util.Scanner;

public class Russia {	
	
	public static void faire_Russie() {
		
		System.out.println("\nVous etes en Russie");
		
		// Enleve les espace inutil et on met en Majuscule la première lettre
		Scanner sr = new Scanner(System.in);
		System.out.println("\nOu souhaitez_vous partir ?");
		String lieu = sr.next();
		String lieuNS = lieu.trim();
		String lieuCap = lieuNS.substring(0,1).toUpperCase() + lieuNS.substring(1);
		
		System.out.println("\nQuel est votre nom ?");
		String nom = sr.next();
		
		System.out.println("\nVotre numéro de téléphone ?");
		String tel = sr.next();
		
		System.out.println("Le client : " + nom + " (" + tel + ") " + "souhaite partir à " + lieuCap);
		
		// obtenir la date actuelle ^plus jour/mois/...
		LocalDateTime time_Before_Travel = LocalDateTime.now().plusWeeks(4).plusDays(5);
			
		System.out.println(String.format("Depart au mieux le %02d/%02d", time_Before_Travel.getDayOfMonth(), time_Before_Travel.getMonthValue()));
		
		sr.close();
	}
}
