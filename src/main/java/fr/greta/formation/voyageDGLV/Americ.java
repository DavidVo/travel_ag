package fr.greta.formation.voyageDGLV;
public class Americ
{
	
	public static double calcul_total(int Monthly_Discount, int Number_of_Day, int Price_per_day, int Flight_Price)
	{
		double Total_Price = calcul_total(Number_of_Day,Price_per_day, Flight_Price);
		
		return Total_Price - (Total_Price * Monthly_Discount / 100);
	}

	public static double calcul_total(int Number_of_Day, int Price_per_day, int Flight_Price)
	
	{
		return (Price_per_day * Number_of_Day) + Flight_Price;
	}
	
	public static void faire_Canada(int Price_per_day, int Flight_Price) 
	{

		System.out.print("\nCombien de temps voulez_vous partir ?\n");
		
		java.util.Scanner sc = new java.util.Scanner(System.in);
		
		int Month_Disc = 20;
		
		int Compteur = 0;
		while (Compteur == 0) 
		{
			
			byte Number_of_Day = (byte) sc.nextInt();

			if (Number_of_Day <= 0 || Number_of_Day > 150) 
			{
				System.out.print("Le nombre de jour doit compris entre 1 et 150 \nVeuillez saisir un autre nombre");
			} else {
				
				double Reduced_Priced = calcul_total(Month_Disc, Number_of_Day, Price_per_day, Flight_Price);
				
				System.out.println("\nVoyage du mois : " + Month_Disc + "% sur " + Number_of_Day + " jours à "
						+ Price_per_day + "\u20ac et " + Flight_Price + "\u20ac d\'avion");
				
				System.out.println(String.format("\nLe prix total est de  %.2f \u20ac\n", Reduced_Priced));

				String Phrase_init = "En cadeau exceptionnel pour vos vacances vous obtenez";

				int Modulo_Semaine = Number_of_Day / 7;

				switch (Modulo_Semaine) 
				{
				case 0:
				case 1:
					break;
				case 2:
					System.out.println(Phrase_init + " 1L de sirop d\'erable");
					break;
				case 3:
				case 4:
					System.out.println(Phrase_init + " balade en skidoo");
					break;
				default:
					System.out.println(Phrase_init + " Un billet pour un match de hockey");
				}
				if (Number_of_Day > 75) {
					System.out.print("Chèque d'accompte necessaire");
				}

				String Mot_Jour = "Jour";
				String quoi;

				int[] Jour_Spectacle = { 5, 11, 13, 22 };
				int Compteur_Jour = 0;

				for (int day = 1; day <= Number_of_Day; day++) 
				{
					if (Compteur_Jour < Jour_Spectacle.length && day == Jour_Spectacle[Compteur_Jour]) 
					{
						System.out.println("Aujourd'hui y'a spectacle");
						Compteur_Jour++;
					}

					if (day == 1 || day == Number_of_Day) 
					{
						quoi = "Avion";
						System.out.println(Mot_Jour + " " + day + " :" + quoi);
					}

					else if ((day % 5) == 0)
					{
						quoi = "C'est le jour de la Poutine";
						System.out.println(Mot_Jour + " " + day + " :" + quoi);
					}

					else
					{
						quoi = "Aujourd'hui c'est ski";
						System.out.println(Mot_Jour + " " + day + " :" + quoi);
					}

				}
				
				Compteur = 1;
			}
		}
		
		sc.close();
	}

	public static void faire_Bresil()
	{
		System.out.println("Vous etes au Bresil");
		int dest = 1;
		System.out.println(calculer_bresil(dest) + "trajets");
		
	}
	
	// Exemple d'une fonction recursive
	public static int calculer_bresil(int Nb_trajets)
	{
		if(Nb_trajets <= 1)
			return 1;
		else
		{
			return Nb_trajets * calculer_bresil(Nb_trajets-1);
		}
	}

}
