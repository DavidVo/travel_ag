package fr.greta.formation.voyageDGLV;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import java.lang.Math;
import java.util.Random;

public class Japon_frame extends JFrame implements MouseListener, ActionListener {
	
	private Color bg;
	
	private JTextField tf;
	private JLabel bl6;
	private JFrame frame;
	private JButton bt;
	private JSpinner jours;
	private JTextField tf2;
	private BlinkLabel bl;
	private JLabel bl2;
	private JLabel bl3;
	private JLabel bl5;
	int compteur = 0;
	
	
	Font font1 = new Font("SansSerif", Font.BOLD, 30);
	Font font2 = new Font("SansSerif", Font.BOLD, 36);
	Font font3 = new Font("SansSerif", Font.PLAIN, 20);
	Font font4 = new Font("SansSerif", Font.BOLD, 42);
	
	public Japon_frame() {
		frame = new JFrame("Japon");
		
		setSize(400,500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Permet d'arreter l'application quand on ferme la fenetre
		getContentPane().setLayout(null); // indique pas de redimmossionnement de la fenetre
		getContentPane().setBackground(Color.pink);
		setLocation(100,500);
		frame.addMouseListener(this);
		

		
		// Créer un bouton
		bt = new JButton("Enregistrer"); 
		getContentPane().add(bt); // Ajoute le bouton 
		bt.setFont(font3);
		bt.setBackground(Color.green);
		bt.setBounds(100,322,200,40);// Defini la position du bouton position et taille du bouton
		//bt.addActionListener(this);
		bt.addMouseListener(this);
	
		// Creation d'un label
		bl = new BlinkLabel("\u2661 Nouveau Voyage \u2661"); 
		getContentPane().add(bl);
		bl.setFont(font2);
		bl.setForeground(Color.yellow);
		bl.setBackground(Color.yellow);
		bl.setBounds(20,0,400,80);
		
		bl2 = new JLabel("Nom"); 
		getContentPane().add(bl2);
		bl2.setFont(font1);
		bl2.setForeground(Color.magenta);
		bl2.setBounds(20,82,200,80);
		
		bl3 = new JLabel("\u260E"); 
		getContentPane().add(bl3);
		bl3.setForeground(Color.magenta);
		bl3.setFont(font1);
		bl3.setBounds(20,162,200,80);
		
		bl5 = new JLabel("Jour"); 
		getContentPane().add(bl5);
		bl5.setFont(font1);
		bl5.setForeground(Color.magenta);
		bl5.setBounds(20,242,200,80);
		
		bl6 = new JLabel("Veuillez tout remplir"); 
		getContentPane().add(bl6);
		bl6.setFont(font1);
		bl6.setForeground(Color.cyan);
		bl6.setBounds(50,382,300,40);
		
		
		
		// Creation d'un textfield
		tf = new JTextField(); 
		getContentPane().add(tf); 
		tf.setBackground(Color.magenta);
		tf.setBounds(400-250,107,200,30);
		// Ajout d'une action en fonction de si il se passe quelquechose dans la fentre créer
		// Pour ça on a implementé MouseListener (interface)
		// on a fait sur this -> Let Japon_frame implement Meth...
		// Puis sur Japon_frame (la classe) Add unimplemented methods
		tf.addMouseListener(this);
		bg = tf.getBackground();
		
		tf2 = new JTextField(); 
		getContentPane().add(tf2);
		tf2.setBackground(Color.magenta);
		tf2.setBounds(400-250,187,200,30);
		
		
		// créer un menu à flêche
		jours = new JSpinner(new SpinnerNumberModel(1,1,100,1));  
		getContentPane().add(jours); 
		jours.setBackground(Color.magenta);
		jours.setBounds(400-250,267,200,30);
		
		// Creation d'un textarea
		//JTextArea bte = new JTextArea("Bonjour"); 
		//getContentPane().add(bte); 
		//bte.setBounds(20,80,100,80);
		
		// Creation d'un checkbox
		//JCheckBox cb = new JCheckBox("Bonjour"); 
		//getContentPane().add(cb); 
		//cb.setBounds(90,200,100,80);
		
	
		// Objet pour stocker les données
		// SpinnerNumberModel(defaut,min,max,pas)
		
		
		//fait apparaitre la fenetre
		setVisible(true);
	}

	
	public void mouseClicked(MouseEvent ac) {
		// TODO Auto-generated method stub
	}
	
	
	
	
	public void mouseEntered(MouseEvent me) {
		// TODO Auto-generated method stub
		
		if (compteur < 10)
		{Random rand = new Random();
		int LocX = rand.nextInt(200);
		
		int LocY = rand.nextInt(460);
	
		bt.setLocation(LocX,LocY);
		System.out.println(LocX + " " + LocY);
		compteur ++;
		System.out.println(compteur);}
		
		else {
		JLabel bll = new JLabel("You lose bitch"); 
		getContentPane().add(bll);
		bll.setFont(font4);
		bll.setForeground(Color.yellow);
		bll.setBounds(30,175,400,80);
		
		bt.setVisible(false);
		bl.setVisible(false);
		bl2.setVisible(false);
		bl3.setVisible(false);
		bl5.setVisible(false);
		bl6.setVisible(false);
		tf.setVisible(false);
		tf2.setVisible(false);
		jours.setVisible(false);}
	}

	
	public void mouseExited(MouseEvent mm) {
		// TODO Auto-generated method stub
		//Component cc = mm.getComponent();
		//cc.setBackground(bg);
		
	}

	
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		//frame.setLocation(100,100);
		bl6.setText("Voyage pour " + tf.getText() + "\nenregistré");
		
	}
}
