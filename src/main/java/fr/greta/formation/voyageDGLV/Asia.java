package fr.greta.formation.voyageDGLV;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import javax.swing.UIManager;

public class Asia {

	public static void faire_Turquie() {

		System.out.println("\nVous etes en Turquie");

		Scanner inp = new Scanner(System.in);
		
		int wayOut = 0;
		
		ArrayList<String> differents_Lieux = new ArrayList<String>();
		
		Map<String, Integer> plus_Cher = new HashMap<String, Integer>();
		plus_Cher.put("Ankara", 1000);
		plus_Cher.put("Adana", 500);
		
		while (wayOut != 1){
			
			
			System.out.println("\nOu souhaitez_vous partir (Pour finir d\'ajouter des étapes saisir EXIT)?");
			String step = inp.next();
			if(step.contentEquals("EXIT")) {
				wayOut = 1;
		
			}
			else {
				differents_Lieux.add(step);
			}
		}
		
		int list_Size = differents_Lieux.size();
		int tot = 0;
		
		for(String ind:differents_Lieux) {
			
			if (plus_Cher.get(ind) != null) {
				System.out.println("Pour " + ind + " il y a un surplus de " + plus_Cher.get(ind));
				tot = tot+plus_Cher.get(ind);
			}
			if (ind.contentEquals(differents_Lieux.get(0))) {
				System.out.println("Vous commencerez par " + ind);
			}
			else if (ind.contentEquals(differents_Lieux.get(list_Size-1))) {
				System.out.println("Et vous terminerez par " + ind);
			}
			else {
				System.out.println("Puis vous passerez par " + ind);
			}
			System.out.println(tot);
			
		}
		
		inp.close();
	}
	
	public static void faire_Japon() {
		
		System.out.println("Bienvenue au Japon");
		
		// definition d'un theme pour les fenetre celui en exemple est moche donc conserve le par defaut
		//try {
		//	UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");	
		//} catch(Exception ex) {}
		new Japon_frame();
		
	}
}
