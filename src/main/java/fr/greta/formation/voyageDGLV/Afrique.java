package fr.greta.formation.voyageDGLV;

import java.io.FileWriter;
import java.util.Scanner;

public class Afrique {

	public static void faire_Namibie() throws RuntimeException { // ici le throws .. indique qu'il y aura peut etre des
																	// exceptions

		Scanner sc = new Scanner(System.in);

		try {

			System.out.println("\nQuel est votre nom ?");
			String nom = sc.next();

			if (nom.contentEquals("JP")) {
				throw new IllegalArgumentException("Ah non non non pas avec nous lui");
			}

			System.out.println("\nVotre adresse email ?");
			String mail = sc.next();

			// ! au debut de la condition permet de l'inverser
			if (!mail.substring(2, mail.length() - 2).contains("@")) {
				System.out.println("Erreur dans l\'adresse email");
			}

			// Methode pour enregistrer en CSV avec un ajout au fur et a mesure grace au
			// true
			FileWriter fw = new FileWriter("n.csv", true);
			fw.write(nom + "," + mail + "\n");
			fw.close();

			// Catch le erreur de type, avec le nom dyu type d'erreur que l'on veut
			// attrapper
		} catch (IllegalArgumentException ex) {
			System.out.print("Erreur: " + ex);

			// Si une exception a eu lieu dans le try mais ne fait pas partie des catch
			// precedent
			// ici IllegalArgumentException alors il ira dans ce catch
		} catch (Exception ex) {
			System.out.print(ex);

			// permet de faire une action quoi qui se passe
		} finally {
			sc.close();
		}

	}

	public static void faire_Madagascar() {

		Scanner sc2 = new Scanner(System.in);

		try {

			System.out.println("\nQuel est votre nom ?");
			String nom = sc2.next();

			System.out.println("\nVotre adresse email ?");
			String mail = sc2.next();
			// ! au debut de la condition permet de l'inverser
			if (!mail.substring(2, mail.length() - 2).contains("@")) {
				throw new Exception("Erreur dans l\'adresse email");
			}

			System.out.println("\nNombre de voyageurs ?");
			int nb_voy = sc2.nextInt();
			if (nb_voy <= 0) {
				throw new Exception("Le nombre de voyageurs doit etre positif");
			}
			
			AfriqueDAO dao = new AfriqueDAO();
			dao.insereDemande(nom,mail, nb_voy);
			dao.afficher_Par_email(mail);
			dao.ferme();

		} catch (Exception ex) {
			System.out.print("Excetpion :" + ex);

			// permet de faire une action quoi qui se passe
		} finally {
			sc2.close();
		}

	}

}
