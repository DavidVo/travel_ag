package fr.greta.formation.voyageDGLV;

import fr.greta.formation.voyageDGLV.donnees.Destination;
import fr.greta.formation.voyageDGLV.donnees.DestinationMaritime;
import fr.greta.formation.voyageDGLV.donnees.Nomable;

public class Europe 
{
	public static void faire_Espagne()
	{
		System.out.println("\nVous etes en Espagne\n");
		
		
		// Sans constructeur et sans getter and setter ne fonctionne donc pas quand attribut est private
		//Destination Des_Bracelone =  new Destination();
		//Des_Bracelone.nom = "Barcelone";
		//Des_Bracelone.pays = "Espagne";
		//Des_Bracelone.jours = 4;
		
		// Quand attributs private utilisation des getter and setters
		Destination Des_Bracelone =  new Destination();
		Des_Bracelone.setNom("Barcelone");
		Des_Bracelone.setPays("Espagne");
		Des_Bracelone.setJours(22);
		
		//System.out.println("Pour " + Des_Bracelone.nom + " en " + Des_Bracelone.pays + " nous conseillons d'y passer " + Des_Bracelone.jours + " jours");
		
		Des_Bracelone.Allonger(10);
		
		String Retour_Chaine = Des_Bracelone.toString();	
		System.out.println(Retour_Chaine);
		
		
		// Avec Constructeur
		Destination Des_Grenade = new Destination("Grenade", "Espagne", 8);
		String Retour_chaine2 = Des_Grenade.toString();
		System.out.println(Retour_chaine2);
		
		// Dans Java le "=" fait une copie par reference, et la "==" verifie si c'est le meme objet
		Destination Des_Gre2 = Des_Grenade;
		System.out.println(Des_Gre2 == Des_Grenade);
		
		/// Vu que c'est une copie par reference, en modifiant un on modifie automatquement l'autre 
		Des_Gre2.setNom("GrenadeBis");
		System.out.println(Des_Grenade.toString());
		
		/// Pour comparer le contenue et pas si c'est le meme objet en Java on utilise ".equals()" 
		Destination Des_Madrid = new Destination("Madrid", "Espagne", 22);
		System.out.println(Des_Madrid.equals(Des_Bracelone));
		
		/// Utilisation du constructeur permettant de faire un clone
		Destination Des_Madridbis = new Destination(Des_Madrid);
		System.out.println(Des_Madridbis);
		System.out.println(Des_Madridbis == Des_Madrid);
	}
		
	/// Utilisation de l'heritage
	public static void faire_Italie()
	{
		System.out.println("Welcome to Italy");
		
		// Utilisation des methode et attributs de la classe mere
		DestinationMaritime Des_Sicile = new DestinationMaritime();
		Des_Sicile.setNom("Syracuse");
		Des_Sicile.setPays("Italie");
		Des_Sicile.setJours(2);
		Des_Sicile.setIle("Sicile");
		System.out.println(Des_Sicile.toString());
		
		
		// utlisation avec le constructeur creer dans la classe fille
		DestinationMaritime Des_Capri = new DestinationMaritime("Capri Town", "Itlaie", 15, "Capri");
		System.out.println(Des_Capri.toString());
		
		//Utilisation de l'interface
		final Nomable desM_Catane;
		desM_Catane = new DestinationMaritime();
		desM_Catane.setNom("Catane");
		
			
	}
}
