package fr.greta.formation.voyageDGLV.donnees;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DestinationTest {
	
	// Cet groupe permet de ne pas mettre pour chaque test le "new Destination
	private Destination d;
	
	@BeforeEach
	void before() {
		d = new Destination();
	}
	
	
	@Test
	void testCtor() {
		//fail("Not yet implemented");
		//Destination d = new Destination();
		assertEquals(0, d.getJours());
		assertEquals("",d.getNom());
	}
	
	//le mieux est de tester 1 par 1 pour savoir exactmeent d'ou vient le pb
	@Test
	void testSetPays() {
		//Destination d = new Destination();
		d.setPays("USA");
		assertEquals("USA",d.getPays());
	}
	
	
	void testAllonger() {
		int JourTest = 2;
		int JourTestBis = -3;
		d.setJours(JourTest);
		d.Allonger(JourTestBis);
		assertEquals(JourTestBis+JourTest, d.getJours());
	}
	

}
